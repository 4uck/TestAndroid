
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestLogin {
    AppiumDriver driver;
    @Test
    public void testLogin() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        // Set android deviceName desired capability. Set it Android Emulator.
        capabilities.setCapability("deviceName", "Android Emulator");
        capabilities.setCapability("platformVersion", "7.0");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("appPackage", "com.perm.kate");
        capabilities.setCapability("appActivity", "com.perm.kate.InitialActivity");


        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        driver.findElement(By.id("com.perm.kate:id/username")).sendKeys("monin.mitya@mail.ru");
        driver.findElement(By.id("com.perm.kate:id/password")).sendKeys("12345");

        Thread.sleep(2000);
//        driver.findElement(By.id("com.android.calculator2:id/del")).click();
//
//        driver.findElement(By.id("com.android.calculator2:id/digit_2")).click();
//        driver.findElement(By.id("com.android.calculator2:id/op_add")).click();
//        driver.findElement(By.id("com.android.calculator2:id/digit_5")).click();
//        //driver.findElement(By.id("com.android.calculator2:id/eq")).click();
//
//        String result = driver.findElement(By.id("com.android.calculator2:id/result")).getText();
//        System.out.println("Number sum result is : " + result);
//        Assert.assertTrue(result.equals("7"));

        driver.quit();
    }
}